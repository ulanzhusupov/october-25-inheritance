import 'package:october_25_inheritance/october_25_inheritance.dart';
import 'package:test/test.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });
}
