// Создайте класс Товар с полями название и цена. Затем создайте класс Продукт,
// который наследует Товар и переопределяет метод описание() для возвращения описания продукта.

class Product {
  String name;
  double price;

  Product({required this.name, required this.price});

  @override
  String toString() {
    // TODO: implement toString
    return "Название продукта: $name\nЦена продукта: $price";
  }
}

class Smartphone extends Product {
  Smartphone({required super.name, required super.price});

  @override
  String toString() {
    return "Название смартфона: $name\nЦена смартфона: $price";
  }
}
