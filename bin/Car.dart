// ignore: file_names

// Создайте класс Автомобиль с полями марка и год. Затем создайте дочерний класс
// ЛегковойАвтомобиль, который наследует Автомобиль и переопределяет метод
// описание() для возвращения описания легкового автомобиля.

class Car {
  String mark;
  int year;

  Car({required this.mark, required this.year});

  String info() {
    return "Марка: $mark, Год: $year";
  }
}

class LightCar extends Car {
  LightCar({required super.mark, required super.year});

  @override
  String info() {
    return "Марка легкового авто: $mark\nГод легкового авто: $year";
  }
}
