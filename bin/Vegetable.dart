// Создайте класс Овощ с полем название. Затем создайте класс Морковь,
// который наследует Овощ и переопределяет метод цвет()
// для возвращения цвета моркови.

class Vegetable {
  String? color;
  Vegetable({required this.color});

  String? get getColor => color;
}

class Carrot extends Vegetable {
  Carrot({required super.color});

  @override
  // TODO: implement getColor
  String? get getColor => "Цвет морковки: ${super.getColor}";
}
