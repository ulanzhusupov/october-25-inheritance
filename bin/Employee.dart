// Создайте класс Сотрудник с полями имя и зарплата. Затем создайте класс Менеджер,
// который наследует Сотрудник и переопределяет метод вывестиИнформацию()
// для вывода информации о менеджере.

class Employee {
  String name;
  int age;
  String position;
  double salary;

  Employee(
      {required this.name,
      required this.age,
      required this.position,
      required this.salary});

  @override
  String toString() {
    // TODO: implement toString
    return "Employee name: $name\nEmployee age: $age\nEmployee position: $position\nEmployee salary: $salary";
  }
}

class Manager extends Employee {
  Manager(
      {required super.name,
      required super.age,
      required super.position,
      required super.salary});

  @override
  String toString() {
    // TODO: implement toString
    return "Manager name: $name\nManager age: $age\nManager position: $position\nManager salary: $salary";
  }
}
