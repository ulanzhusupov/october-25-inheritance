// Создайте класс МузыкальныйИнструмент с полем название.
// Затем создайте класс Гитара, который наследует МузыкальныйИнструмент и
// переопределяет метод звук() для воспроизведения звука гитары.

class MusicalInstrument {
  String name;

  MusicalInstrument({required this.name});

  void zvuk() {}
}

class Guitar extends MusicalInstrument {
  Guitar({required super.name});

  @override
  void zvuk() {
    print("Звук гитары");
  }
}
