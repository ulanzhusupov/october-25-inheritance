// Создайте класс Животное с полями имя и возраст. Затем создайте класс Собака,
// который наследует Животное и переопределяет метод голос() для возвращения звука,
// который издает собака.

class Animal {
  String name;
  int age;
  Animal({required this.name, required this.age});

  void golos() {
    print("Какой-то голос");
  }
}

class Dog extends Animal {
  Dog({required super.name, required super.age});

  @override
  void golos() {
    print("ГАф гаф");
  }
}
