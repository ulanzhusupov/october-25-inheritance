// Создайте класс Фигура с полями цвет и площадь. Затем создайте класс Круг,
// который наследует Фигура и переопределяет метод площадь() для вычисления площади круга.

import 'dart:math';

class Figure {
  String color;
  Figure({required this.color});

  double getPloshad() {
    return 0.0;
  }
}

class Circle extends Figure {
  double radius;
  Circle({required super.color, required this.radius});

  @override
  double getPloshad() {
    // TODO: implement getPloshad
    return pi * (radius * radius);
  }
}
