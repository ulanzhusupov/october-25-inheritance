// Создайте класс Здание с полем адрес. Затем создайте класс Дом,
// который наследует Здание и переопределяет метод типЗдания() для указания,
// что это дом.

class Building {
  String address;

  Building({required this.address});

  void buildingType() {
    print("Building type: building");
  }
}

class Home extends Building {
  Home({required super.address});

  @override
  void buildingType() {
    // TODO: implement buildingType
    print("Building type: $runtimeType");
  }
}
