// Создайте класс Организм с полем имя. Затем создайте класс Человек,
// который наследует Организм и переопределяет метод приветствие()
// для приветствия человека и вывода его имени и возраста.

abstract class Organism {
  void greeting();
}

class Human extends Organism {
  String name;
  int age;

  Human({required this.name, required this.age});

  @override
  void greeting() {
    // TODO: implement greeting
    print("Привет! Меня зовут $name, мне $age лет.");
  }
}
